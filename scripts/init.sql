SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `tag`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `{$prfx}tag` ;

CREATE  TABLE IF NOT EXISTS `{$prfx}tag` (
  `{$prfx}tag_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `object_type_id` TINYINT UNSIGNED NOT NULL ,
  `title` VARCHAR(255) NOT NULL ,
  `name` VARCHAR(255) NULL ,
  `title_plural` VARCHAR(255) NULL ,
  `title_accusative` VARCHAR(255) NULL ,
  `sort_order` INT UNSIGNED NULL ,
  PRIMARY KEY (`{$prfx}tag_id`) ,
  INDEX `fk_{$prfx}tag_object_type_id_idx` (`object_type_id` ASC) ,
  UNIQUE INDEX `unq_{$prfx}tag_title` (`object_type_id` ASC, `title` ASC)
--  CONSTRAINT `fk_{$prfx}tag_object_type_id`
--    FOREIGN KEY (`object_type_id` )
--    REFERENCES `{$prfx}object_type` (`{$prfx}object_type_id` )
--    ON DELETE CASCADE
--    ON UPDATE CASCADE
) ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `object_has_tag`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `{$prfx}object_has_tag` ;

CREATE  TABLE IF NOT EXISTS `{$prfx}object_has_tag` (
  `object_type_id` TINYINT UNSIGNED NOT NULL ,
  `object_id` INT UNSIGNED NOT NULL ,
  `{$prfx}tag_id` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`object_type_id`, `object_id`, `{$prfx}tag_id`) ,
  INDEX `fk_{$prfx}object_has_tag_{$prfx}tag_id_idx` (`{$prfx}tag_id` ASC) ,
  INDEX `fk_{$prfx}object_has_tag_object_id_idx` (`object_id` ASC) ,
  INDEX `fk_{$prfx}object_has_tag_object_type_id_idx` (`object_type_id` ASC) ,
--  CONSTRAINT `fk_{$prfx}object_has_tag_object_id`
--    FOREIGN KEY (`object_id` )
--    REFERENCES `{$prfx}object` (`{$prfx}object_id` )
--    ON DELETE CASCADE
--    ON UPDATE CASCADE,
  CONSTRAINT `fk_{$prfx}object_has_tag_{$prfx}tag_id`
    FOREIGN KEY (`{$prfx}tag_id` )
    REFERENCES `{$prfx}tag` (`{$prfx}tag_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE
--  CONSTRAINT `fk_{$prfx}object_has_tag_object_type_id`
--    FOREIGN KEY (`object_type_id` )
--    REFERENCES `{$prfx}object_type` (`{$prfx}object_type_id` )
--    ON DELETE CASCADE
--    ON UPDATE CASCADE
) ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

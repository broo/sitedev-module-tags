<?php

namespace Core\Sitedev\Tags\Model;

use Ext\Db;
use Ext\File;
use Ext\String;
use Ext\Xml;

abstract class Tag extends \App\ActiveRecord
{
    const FOLDER = 'tags';

    /**
     * Свойство нужно объявить в классе детей.
     *
     * Sitedev\Tags\Model\Tag[]
     */
    protected static $_list;

    public function __construct()
    {
        parent::__construct('tag');

        $this->addPrimaryKey('integer');
        $this->addAttr('object_type_id', 'integer');
        $this->addAttr('title', 'string');
        $this->addAttr('name', 'string');
        $this->addAttr('title_plural', 'string');
        $this->addAttr('title_accusative', 'string');
        $this->addAttr('is_published', 'boolean');
        $this->addAttr('sort_order', 'integer');
    }

    protected function _checkName()
    {
        if ($this->name == '') {
            $this->name = File::normalizeName($this->getTitle());
        }
    }

    public function create()
    {
        if (!$this->_objectTypeId) {
            $this->_objectTypeId = static::OBJECT_TYPE_ID;
        }

        $this->_checkName();
        return parent::create();
    }

    public function update()
    {
        $this->_checkName();
        return parent::update();
    }

    public function delete()
    {
        $this->deleteTagObjects($this->id);
        return parent::delete();
    }

    public function getFilePath()
    {
        return DOCUMENT_ROOT . 'f/' . self::FOLDER . '/' . $this->id . '/';
    }

    /**
     * @param array $_where
     * @param array $_params
     * @return self[]
     */
    public static function getList($_where = null, $_params = null)
    {
        $where = empty($_where) ? array() : $_where;
        $where['object_type_id'] = static::OBJECT_TYPE_ID;

        return parent::getList($where, $_params);
    }

    public static function getTagWithObjectIds()
    {
        $p = Db::get()->getPrefix();
        $typeId = Db::escape(static::OBJECT_TYPE_ID);

        return Db::get()->getList("
            SELECT   `{$p}tag_id`
            FROM     `{$p}object_has_tag`
            WHERE    `{$p}object_type_id` = $typeId
            GROUP BY `{$p}tag_id`
        ");
    }

    public static function getCount($_where = null)
    {
        $where = empty($_where) ? array() : $_where;
        $where['object_type_id'] = static::OBJECT_TYPE_ID;

        return parent::getCount($where);
    }

    public static function getObjectTags($_id)
    {
        $p = Db::get()->getPrefix();
        $id = Db::escape($_id);
        $typeId = Db::escape(static::OBJECT_TYPE_ID);
        $class = get_called_class();
        $tags = array();

        $list = Db::get()->getList("
            SELECT
                `t`.*
            FROM
                `{$p}tag` AS `t`,
                `{$p}object_has_tag` AS `l`
            WHERE
                `l`.`{$p}object_type_id` = $typeId AND
                `l`.`object_id` = $id AND
                `l`.`{$p}tag_id` = `t`.`{$p}tag_id`
        ");

        foreach ($list as $item) {
            /** @var self $tag */
            $tag = new $class;
            $tag->fillWithData($item);
            $tags[$tag->id] = $tag;
        }

        return $tags;
    }

    public static function updateObjectTags($_id, array $_tagIds)
    {
        $p = Db::get()->getPrefix();
        static::deleteObjectTags($_id);

        foreach ($_tagIds as $id) {
            $attrs = Db::get()->getQueryFields(array(
                'object_type_id' => static::OBJECT_TYPE_ID,
                'object_id' => $_id,
                $p . 'tag_id' => $id
            ), 'insert');

            Db::get()->execute("INSERT INTO `{$p}object_has_tag` $attrs");
        }

        return true;
    }

    public static function deleteObjectTags($_id)
    {
        $p = Db::get()->getPrefix();
        $id = Db::escape($_id);
        $typeId = Db::escape(static::OBJECT_TYPE_ID);

        Db::get()->execute("
            DELETE FROM `{$p}object_has_tag`
            WHERE `{$p}object_type_id` = $typeId AND `object_id` = $id
        ");
    }

    public static function deleteTagObjects($_id)
    {
        $p = Db::get()->getPrefix();
        $id = Db::escape($_id);

        Db::get()->execute("
            DELETE FROM `{$p}object_has_tag`
            WHERE `{$p}tag_id` = $id
        ");
    }

    public static function getTagObjectIds($_id)
    {
        $p = Db::get()->getPrefix();

        $cond = implode(' AND ', Db::get()->getWhere(array(
            $p . 'tag_id' => $_id,
            $p . 'object_type_id' => static::OBJECT_TYPE_ID
        )));

        if (is_array($_id)) {
            $cond .= ' GROUP BY `object_id`';
        }

        return Db::get()->getList("
            SELECT `object_id` FROM `object_has_tag` WHERE $cond
        ");
    }

    public static function computeUniqueTitle($_title)
    {
        return String::toLower($_title);
    }

    public function getUniqueTitle()
    {
        return static::computeUniqueTitle($this->title);
    }

    public static function getOrCreateIfUnique($_title)
    {
        if (!isset(static::$_list)) {
            static::$_list = array();

            foreach (static::getList() as $tag) {
                static::$_list[$tag->getUniqueTitle()] = $tag;
            }
        }

        $title = static::computeUniqueTitle($_title);

        if (isset(static::$_list[$title])) {
            return static::$_list[$title];

        } else {
            $class = get_called_class();
            /** @var self $tag */
            $tag = new $class;
            $tag->title = $_title;
            $tag->save();

            static::$_list[$tag->getUniqueTitle()] = $tag;

            return $tag;
        }
    }

    /**
     * @return \App\Cms\Back\Office\NavFilter
     */
    public static function getCmsNavFilter()
    {
        $filter = new \App\Cms\Back\Office\NavFilter(get_called_class());
        $filter->isSortable(true);

        $filter->addElement(
            new \App\Cms\Back\Office\NavFilter\Element('title', 'Название')
        );

        $filter->run();

        return $filter;
    }

    public function getXml($_node = null, $_xml = null, $_attrs = null)
    {
        if (empty($_xml))         $xml = array();
        else if (is_array($_xml)) $xml = $_xml;
        else                      $xml = array($_xml);

        Xml::append($xml, Xml::notEmptyCdata(
            'plural',
            $this->titlePlural
        ));

        Xml::append($xml, Xml::notEmptyCdata(
            'accusative',
            $this->titleAccusative
        ));

        return parent::getXml($_node, $xml, $_attrs);
    }

    public function isPublished()
    {
        return (bool) $this->isPublished;
    }
}
